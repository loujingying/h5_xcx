# 微信小程序封装H5域名
#### 介绍
使用微信小程序web-view组件进行封装H5网页，最快的速度完成展示型小程序。
注：web-view组件不支持个人小程序，只能认证过的企业小程序。（认证不认证都可以使用）

#### qq群交流：326306049



#### 安装教程

1.  首先域名为https站点。
2.  导入代码修改appid
3.  修改page/index.wxml

```
<!--index.wxml-->
<web-view src="https://www.yxxxm.com/"></web-view> //修改为自己的域名
```
4. 修改page/index.js

```
   /**
   * 分享当前页面
   */
  onShareAppMessage() {
    return {
      title: '云信网络科技有限公司',//这里填写title
      path: "/pages/index/index"//这里填写路径
    };
  },
```

![输入图片说明](https://images.gitee.com/uploads/images/2020/1204/084151_5b70e546_902699.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1204/084225_2b5115ca_902699.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1204/084246_5244bdfc_902699.png "屏幕截图.png")


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
