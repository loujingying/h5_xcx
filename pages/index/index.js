//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
 

  },

     /**
   * 分享当前页面
   */
  onShareAppMessage() {
    return {
      title: '云信网络科技有限公司',//这里填写title
      path: "/pages/index/index"//这里填写路径
    };
  },
   //用户点击右上角分享朋友圈 //使用web-view页面不支持分享朋友圈。。
   onShareTimeline:function(res) {
    return {
      title: '云信网络科技有限公司',//这里填写title
      path: "/pages/index/index"//这里填写路径
    }
  },

})
